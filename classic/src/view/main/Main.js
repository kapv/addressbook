/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('AddressBook.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'Ext.layout.Fit',
        'AddressBook.view.main.MainController',
        'AddressBook.view.main.MainModel',
        'AddressBook.view.main.List'
    ],

    controller: 'main',
    viewModel: 'main',

    ui: 'navigation',


    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },

        iconCls: 'x-fa fa-address-book'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding:0,
        tabConfig: {

            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    listeners:{
        tabchange:function(tabPanel,tab){
            if (tab.id=='tabHome'){
                Ext.getStore('addresses').load();
            }
        }
    },

    items: [{
        title: 'Home',
        iconCls: 'x-fa fa-home',
        xtype:'panel',
        layout:'fit',
        id:'tabHome',
        items: [{
            xtype:'mainlist'
        }]
    }, {
        title: 'Map',
        iconCls: 'x-fa fa-map-marker',
        layout:'fit',
        items:[ {xtype:'viewMap'}
        ]
    }]



});
