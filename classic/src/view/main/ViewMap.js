Ext.define('AddressBook.view.main.ViewMap', {
    extend: 'Ext.Panel',
    xtype: 'viewMap',
    title:'Map',
    requires: [
        'Ext.ux.LeafletMap'
    ],

    layout: {
        type: 'fit'
    },

    dockedItems:[
        {
            xtype : 'toolbar',
            docked: 'top',
            items:[
                {xtype:'button',text:'Create', iconCls:'x-fa fa-plus-circle',listeners:{click:function(){
                    if (this.getText()=='Create'){
                        this.setText("Cancel");
                        this.setIconCls('x-fa fa-times-circle');
                        this.setUserCls("butActive");
                        Ext.getCmp('map1').setEditable(true);
                    }
                    else {
                        this.setText("Create");
                        this.setIconCls('x-fa fa-plus-circle');
                        this.setUserCls(null);
                        Ext.getCmp('map1').setEditable(false);
                    }

                }}}
            ]
        }
    ],

    config: {

        items: [{
            xtype: 'leafletmap',
            id: 'map1',
            useLocation: true,
            autoCenter: true,
            enableOwnPositionMarker: true,
            editCallback:function(lat,lng,address,completeCallback){
                var win=Ext.create('AddressBook.view.main.AddressForm',{
                    title:'New Address',
                    completeCallback:completeCallback
                });
                win.show();
                Ext.getCmp('addressForm').getForm().setValues({
                    latitude:lat,
                    longitude:lng,
                    address:address
                });
            },
            mapOptions: {
                zoom: 14
            }
        }]
    },
    listeners:{
        render:function(){
            Ext.getStore('addresses').load({
                callback: function(records, operation, success) {
                    if (success){
                        Ext.getCmp('map1').setMarkers(records);
                    }
                    else {
                        Ext.Msg.alert("Error","Unable to get addresses!");
                    }
                },
                scope: this
            })
        }
    }
});