/**
 * This view is an example list of people.
 */
Ext.define('AddressBook.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',
    id:'list',
    requires: [
        'AddressBook.store.Addresses'
    ],

    title: 'Addresses',

    store: {
        type: 'addresses'
    },


    columns: [
        { text: 'Name',  dataIndex: 'name' },
        { text: 'Address', dataIndex: 'address', flex: 1 },
        { text: 'Phone', dataIndex: 'phone', flex: 1 }
    ],

    listeners: {
        itemdblclick: 'onItemSelected',
        render:{
            scope: this,
            fn: function(grid) {
                grid.getStore().load();
            }
        }
    },
    dockedItems:[
        {
            xtype : 'toolbar',
            docked: 'top',
            items:[
                {xtype:'button',text:'Refresh',listeners:{click:function(){
                    Ext.getStore('addresses').load();
                }}},
                {xtype:'button',text:'New', iconCls:'x-fa fa-plus-circle',listeners:{click:'onCreate'}},
                {xtype:'button',text:'Edit', iconCls:'x-fa fa-edit',listeners:{click:function(){
                    var controller=new AddressBook.view.main.MainController();
                    var grid=Ext.getCmp('list');
                    var recs=grid.getSelectionModel().getSelection();
                    if (recs.length>0){
                        controller.onEdit(recs[0]);
                    }
                    else {
                        Ext.Msg.alert("Warning!","Address not selected!");
                    }
                }}},
                {xtype:'button',text:'Delete', iconCls:'x-fa fa-trash',listeners:{click:function(){
                    var grid=Ext.getCmp('list');
                    var recs=grid.getSelectionModel().getSelection();
                    if (recs.length>0){
                        var controller=new AddressBook.view.main.MainController();
                        controller.onDelete(recs[0]);

                    }
                    else {
                        Ext.Msg.alert("Warning!","Address not selected!");
                    }
                }}}
            ]
        }
    ]

});
