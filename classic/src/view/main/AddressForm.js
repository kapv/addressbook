Ext.define('AddressBook.view.main.AddressForm', {
    extend: 'Ext.window.Window',
    xtype: 'form-address',
    id:'addressWin',

    layout: 'fit',
    plain: true,
    modal:true,

    responsiveConfig:{
        wide:{
            width: 500,
            height: 300,
            minWidth: 300,
            minHeight: 220
        },
        tall:{
            width:'100%',
            height:'100%'
        }
    },

    items: [{
        xtype: 'form',
        id:'addressForm',
        jsonSubmit:true,
        defaultType: 'textfield',
        fieldDefaults: {
            labelWidth: 60
        },

        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        bodyPadding: 5,
        border: false,

        items: [{
            hideLabel: true,
            emptyText: 'Name',
            name: 'name'
        }, {
            hideLabel: true,
            emptyText: 'Phone',
            name: 'phone'
        }, {
            xtype: 'textarea',
            hideLabel: true,
            name: 'address',
            emptyText: "Address",
            flex: 1
        }, {
            xtype:'hidden',name:'latitude'
        }, {
            xtype:'hidden',name:'longitude'
        }]
    }],

    buttons: [{
        text: 'Save',
        listeners:{click:function(){

            var win=Ext.getCmp('addressWin');

            var form=Ext.getCmp('addressForm').getForm();
            if (win.title.indexOf('New')==0){
                var values=form.getValues();
                var record=Ext.create('AddressBook.model.Addresses',values);//form.getValues();
                delete record.data['id'];
                record.save({
                    success:function(record,operation){
                        Ext.getStore('addresses').add(record);
                        if (win.completeCallback) win.completeCallback();
                    }
                });
            }
            else {
                var record=form.getRecord();
                record.set(form.getValues());
                record.save();
            }
            if (win) win.destroy();
        }}
    }, {
        text: 'Cancel',
        listeners:{click:function(){
            var win=Ext.getCmp('addressWin');
            if (win) win.destroy();
        }}
    }],

});