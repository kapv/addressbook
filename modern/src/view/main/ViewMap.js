Ext.define('AddressBook.view.main.ViewMap', {
    extend: 'Ext.Panel',
    xtype: 'viewMap',
    title:'Map',
    requires: [
        'Ext.ux.LeafletMap'
    ],

    layout: {
        type: 'fit'
    },



    items: [{
        xtype: 'leafletmap',
        id: 'map1',
        useLocation: true,
        autoCenter: true,
        enableOwnPositionMarker: true,
        editCallback:function(lat,lng,address,completeCallback){
            var win=Ext.create('AddressBook.view.main.AddressForm',{
                title:'New Address',
                completeCallback:completeCallback
            });
            win.show();
            Ext.getCmp('addressForm').setValues({
                latitude:lat,
                longitude:lng,
                address:address
            });
        },
        mapOptions: {
            zoom: 14
        }
    },{
        xtype : 'toolbar',
        docked: 'bottom',
        items:[
            {xtype:'button',text:'Create', iconCls:'x-fa fa-plus-circle',handler:function(){
                if (this.getText()=='Create'){
                    this.setText("Cancel");
                    this.setIconCls('x-fa fa-times-circle');
                    this.setUserCls("butActive");
                    Ext.getCmp('map1').setEditable(true);
                }
                else {
                    this.setText("Create");
                    this.setIconCls('x-fa fa-plus-circle');
                    this.setUserCls(null);
                    Ext.getCmp('map1').setEditable(false);
                }

            }}
        ]
    }]
});