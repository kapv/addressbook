/**
 * This view is an example list of people.
 */
Ext.define('AddressBook.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',
    id:'list',
    requires: [
        'AddressBook.store.Addresses'
    ],

    title: 'Addresses',

    store: {
        type: 'addresses'
    },


    columns: [
        { text: 'Name',  dataIndex: 'name' },
        { text: 'Address', dataIndex: 'address', flex: 1 },
        { text: 'Phone', dataIndex: 'phone', flex: 1 }
    ],

    onRender:function(){
        this.callParent();
        this.getStore().load();
    },



});
