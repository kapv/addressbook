/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 */
Ext.define('AddressBook.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',
        'Ext.layout.Fit',
        'AddressBook.view.main.MainController',
        'AddressBook.view.main.MainModel',
        'AddressBook.view.main.List'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        }
    },

    tabBarPosition: 'bottom',
    tabBar:{
        tabchange:function(tabPanel,tab){
            if (tab.id=='tabHome'){
                Ext.getStore('addresses').load();
            }
        }
    },

    items: [{
        title: 'Home',
        iconCls: 'x-fa fa-home',
        xtype:'panel',
        layout:'fit',
        id:'tabHome',
        items: [{
            xtype : 'toolbar',
            docked: 'bottom',
            items:[
                {xtype:'button',text:'Refresh',handler:function(){
                    Ext.getStore('addresses').load();
                }},
                {xtype:'button',text:'New', iconCls:'x-fa fa-plus-circle',handler:'onCreate'},
                {xtype:'button',text:'Edit', iconCls:'x-fa fa-edit',handler:function(){
                    var controller=new AddressBook.view.main.MainController();
                    var grid=Ext.getCmp('list');
                    var recs=grid.getSelection();
                    if (recs){
                        controller.onEdit(recs);
                    }
                    else {
                        Ext.Msg.alert("Warning!","Address not selected!");
                    }
                }},
                {xtype:'button',text:'Delete', iconCls:'x-fa fa-trash',handler:function(){
                    var grid=Ext.getCmp('list');
                    var recs=grid.getSelection();
                    if (recs){
                        var controller=new AddressBook.view.main.MainController();
                        controller.onDelete(recs);

                    }
                    else {
                        Ext.Msg.alert("Warning!","Address not selected!");
                    }
                }}
            ]
        },{
            xtype:'mainlist'
        }]
    }, {
        title: 'Map',
        iconCls: 'x-fa fa-map-marker',
        layout:'fit',
        id:'tabMap',
        items:[ {xtype:'viewMap'}
        ]
    }],
    listeners:{
        activeitemchange: function(sender,tab) {
            if (tab.id=='tabMap'){
                Ext.getStore('addresses').load({
                    callback: function(records, operation, success) {
                        if (success){
                            Ext.getCmp('map1').setMarkers(records);
                        }
                        else {
                            Ext.Msg.alert("Error","Unable to get addresses!");
                        }
                    },
                    scope: this
                })
            }
        }
    }

});
