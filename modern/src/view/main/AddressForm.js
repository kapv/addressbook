Ext.define('AddressBook.view.main.AddressForm', {
    extend: 'Ext.window.Window',
    xtype: 'form-address',
    id:'addressWin',

    layout: 'fit',
    plain: true,
    modal:true,

    responsiveConfig:{
        wide:{
            width: 500,
            height: 400,
            minWidth: 300,
            minHeight: 320
        },
        tall:{
            width:'100%',
            height:'100%'
        }
    },

    items: [{
        xtype: 'formpanel',
        id:'addressForm',
        jsonSubmit:true,
        defaultType: 'textfield',
        fieldDefaults: {
            labelWidth: 60
        },
        defaults: {
            errorTarget: 'under'
        },
        layout: {
            type: 'vbox',
            align: 'stretch'
        },

        bodyPadding: 5,
        border: false,

        items: [{
            hideLabel: true,
            xtype: 'textfield',
            label: 'Name',
            name: 'name',
            placeholder: 'Mr. Ivan Petrov'
        }, {
            xtype: 'textfield',
            hideLabel: true,
            label: 'Phone',
            name: 'phone',
            placeholder: '(xxx) xxx-xxxx'
        }, {
            xtype: 'textareafield',
            hideLabel: true,
            name: 'address',
            label: "Address",
            flex: 1
        }, {
            xtype:'hiddenfield',name:'latitude'
        }, {
            xtype:'hiddenfield',name:'longitude'
        }]
    }],

    buttons: [{
        text: 'Save',
        handler:function(){

            var win=Ext.getCmp('addressWin');

            var form=Ext.getCmp('addressForm');
            if (win.getTitle().indexOf('New')==0){
                var values=form.getValues();
                var record=Ext.create('AddressBook.model.Addresses',values);//form.getValues();
                delete record.data['id'];
                record.save({
                    success:function(record,operation){
                        Ext.getStore('addresses').add(record);
                        if (win.completeCallback) win.completeCallback();
                        if (win) win.destroy();
                    },
                    failure:function(record,operation){
                        var reader=this.getProxy().getReader();
                        form.setErrors(reader.rawData.data[0]);
                    }
                });
            }
            else {
                var record=form.getRecord();
                record.set(form.getValues());
                record.save({
                    success:function(record,operation){
                        if (win) win.destroy();
                    },
                    failure:function(record,operation){
                        var reader=this.getProxy().getReader();
                        form.setErrors(reader.rawData.data[0]);
                    }
                });

            }

        }
    }, {
        text: 'Cancel',
        handler:function(){
            var win=Ext.getCmp('addressWin');
            if (win) win.destroy();
        }
    }],

});