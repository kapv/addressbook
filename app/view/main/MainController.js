/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('AddressBook.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        this.onEdit(record);
    },


    onCreate:function(){
        var win=Ext.create('AddressBook.view.main.AddressForm',{title:'New Address'});
        win.show();
    },

    onEdit:function(rec){
        var win=Ext.create('AddressBook.view.main.AddressForm',{title:'Edit Address'});
        win.show();
        var form=Ext.getCmp('addressForm');
        form.setRecord(rec);
    },


    onDelete:function(rec){
        Ext.Msg.confirm('Confirm', 'Address will be deleted! Are you sure?', function(choice){
            if (choice === 'yes') {
                rec.erase();
            }
        });
    }



});
