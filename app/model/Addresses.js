Ext.define('AddressBook.model.Addresses', {
    extend: 'AddressBook.model.Base',

    fields: [
        {name:'name',type:'string'},
        {name:'address',type:'string'},
        {name:'latitude', type:'float'},
        {name:'longitude', type:'float'},
        {name:'phone',type:'string'}
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'api/addresses/fetch',
            create: 'api/addresses/add',
            update: 'api/addresses/update',
            destroy: 'api/addresses/delete'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            rootProperty: 'data',
            messageProperty: 'message',
            keepRawData:true
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            rootProperty: 'data'
        }
    }
});