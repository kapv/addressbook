Ext.define('AddressBook.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'AddressBook.model'
    }
});
