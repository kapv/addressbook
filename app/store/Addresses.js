Ext.define('AddressBook.store.Addresses', {
    extend: 'Ext.data.Store',
    storeId:'addresses',
    alias: 'store.addresses',

    model: 'AddressBook.model.Addresses',

});
