/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */


Ext.application({
    extend: 'AddressBook.Application',

    name: 'AddressBook',

    requires: [
        // This will automatically load all classes in the AddressBook namespace
        // so that application classes do not need to require each other.
        'AddressBook.*'
    ],

    // The name of the initial view to create.
    mainView: 'AddressBook.view.main.Main'
    /*launch:function(){
        this.setMainView('AddressBook.view.main.Main');
    }*/

});
